# kimun-kubernetes
Daily use in Kubernetes, scripting shell and more

{Docker Bulid} ->  [image + app / new image] -> {yaml with the new img} -> [deploy over one pod]

## Container
A Docker container image is a lightweight, standalone, executable package of software that includes everything needed to run an application: code, runtime, system tools, system libraries and settings. Container images become containers at runtime and in the case of Docker containers - images become containers when they run on Docker Engine.

## Application
It's your software with your logic

## Kubernetes
Kubernetes is an open-source container-orchestration system for automating computer application deployment, scaling, and management. It was originally designed by Google and is now maintained by the Cloud Native Computing Foundation

## Installing kubectl
This is one console client to Kubernetes
* [Install Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* [Install Kubectl in GNU Linux](https://snapcraft.io/install/kubectl/mint)

```bash
sudo apt update
sudo apt install snapd
sudo snap install kubectl --classic
```

## Alternativas para cluster locales de Kubernetes

### K3S
* https://k3s.io/

```
sudo curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode 644
```

### Minikube
minikube quickly sets up a local Kubernetes cluster on macOS, Linux, and Windows. We proudly focus on helping application developers and new Kubernetes users.

* [MiniKube Docs](https://minikube.sigs.k8s.io/docs/)

* [Install your cluster locally](https://youtu.be/6e_sXAx7kts)
* [MuniKube Start](https://minikube.sigs.k8s.io/docs/start/)

* Install
For Linux users, we provide 3 easy download options (for each architecture):
x86. Binary download
```bash
 curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
 sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

Debian package
```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
```
RPM package
```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm
sudo rpm -ivh minikube-latest.x86_64.rpm
```
ARM
Binary download
```bash
 curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-arm64
 sudo install minikube-linux-arm64 /usr/local/bin/minikube
```
Debian package
```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_arm64.deb
sudo dpkg -i minikube_latest_arm64.deb
```
RPM package
```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-latest.aarch64.rpm
sudo rpm -ivh minikube-latest.aarch64.rpm
```

* How to use -  start your cluster
This is a App to test locally Kubernetes
```bash
minikube start
```
This command configure kubectl to apply the commands over thar Kubernetes instance.
you need VirtualBox installed in your machine


* list of features
minikube addons list
```bash
```
* get into Minikube VM
```bash
minikube ssh
```

* get minikube ip
```bash
 minikube ip
```

* Example how to use minikube
[Deployment in Kubernetes](https://youtu.be/q-ZicDSb3Cc)

## Installing Kubeadm
Kubeadm performs the actions necessary to get a minimum viable cluster up and running.
```bash
sudo snap install kubeadm
```

## Complements
base64 to encode or decode certificates
```bash
apt install cl-base64 -y
```
# Kubeadm commands
* [Kubeadm](https://kubernetes.io/docs/reference/setup-tools/kubeadm)

* Kubeadm token
This command will create a bootstrap token for you
```bash
kubeadm --kubeconfig=kuernetes-config.yaml token create
```

# Kubectl commnads
* list namespaces
```bash
kubectl --kubeconfig=kuernetes-config.yaml get ns
```

* check de config:
```bash
 kubectl --kubeconfig=my-config.yaml config view
```

* get secrets
```bash
kubectl get secrets
```

* get deployments
```bash
kubectl --kubeconfig=kuernetes-config.yaml get deployment
```

* deploy image
```bash
kubectl --kubeconfig=kuernetes-config.yaml create deployment hola-php --image=alpine:test001
```

*get nodes (of kubernetes)
```bash
kubectl --kubeconfig=kuernetes-config.yaml get nodes
```

* get all information
```bash
kubectl --kubeconfig=kuernetes-config.yaml get all
```

* scale
```bash
kubectl scale --replicas=5 {image-name}
```

* get pods
```bash
kubectl get pods
```

* get info about app with the leabel name
```bash
kubectl apply -f 02-hello-app-service-node-port.yaml
```

* get information about the app
```bash
kubectl describe svc hello
```
* update app version in pods
```bash
kubectl apply -f 03-hello-app-deployment.yaml
```
to validate we can consult with `curl`.
```bash
curl http://192.168.99.100:30000
```

# Administradores de Cluster de Kubernetes:
* https://k9scli.io/
* https://www.portainer.io/

# Links
* [Gitlab CI/CD with Kubernetes](https://youtu.be/c5T0UkuD-6g)
* [Boludeando con Docker - GITLAB CI/CD! - PARTE 1](https://www.youtube.com/watch?v=RhSkUh7cGqc)
* [Kubernetes secrets and configmaps](https://opensource.com/article/19/6/introduction-kubernetes-secrets-and-configmaps)
