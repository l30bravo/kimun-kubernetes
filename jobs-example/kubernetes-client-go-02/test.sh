IMAGE_TO_MOVE="demo-job-with-envs-"
NEXUS_HOST="nexus.local.cl"
NEXUS_SNAPSHOT="8085"

build(){
	#go build -o app .
	GOOS=linux go build -o ./app .
	docker build -t ${IMAGE_TO_MOVE} .
}

upload_img(){
	echo "Creando tag [ ${NEXUS_HOST}:${NEXUS_SNAPSHOT}/${IMAGE_TO_MOVE} ]"
	docker tag ${IMAGE_TO_MOVE} "${NEXUS_HOST}:${NEXUS_SNAPSHOT}/${IMAGE_TO_MOVE}"
	docker images
	sleep 3
	docker login -u ${NEXUS_USER} -p ${NEXUS_PAAS} ${NEXUS_HOST}:${NEXUS_SNAPSHOT}
	echo "uploading "${IMAGE_TO_MOVE}
	docker push ${NEXUS_HOST}:${NEXUS_SNAPSHOT}/${IMAGE_TO_MOVE}
}


deploy(){
	kubectl apply -f ../demo_deploy_job_triggerl.yaml
}

### MAIN ###
echo "1 - build"
echo "2 - upload image"
echo "3 - deploy image"
read OPT
if [ $OPT -eq 1 ]; then
	build
fi
if [ $OPT -eq 2 ]; then
	upload_img
fi
if [ $OPT -eq 3 ]; then
	deploy
fi
