package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"strings"
	"time"

	"google.golang.org/protobuf/types/known/timestamppb"
	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubernetes "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

func connectToK8s() *kubernetes.Clientset {
	config, err := rest.InClusterConfig()
	if err != nil {
		fmt.Printf("Error: %#v", err)
		panic(err.Error())
	}

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		fmt.Printf("Error: %#v", err)
		panic(err.Error())
	}

	return clientset
}

func launchK8sJob(clientset *kubernetes.Clientset, jobName string, image string, cmd string) {
	jobs := clientset.BatchV1().Jobs("default")
	var backOffLimit int32 = 0
	var secondAfterFinished int32 = 360

	ts := timestamppb.Now()
	tag := fmt.Sprintf("%d", ts.Seconds)
	jobName += "-" + tag

	jobSpec := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      jobName,
			Namespace: "default",
		},
		Spec: batchv1.JobSpec{
			TTLSecondsAfterFinished: &secondAfterFinished,
			Template: v1.PodTemplateSpec{
				Spec: v1.PodSpec{
					Containers: []v1.Container{
						{
							Name:    jobName,
							Image:   image,
							Env:     []v1.EnvVar{{Name: "IDRCA", Value: "1234"}},
							Command: strings.Split(cmd, " "),
						},
					},
					RestartPolicy: v1.RestartPolicyNever,
				},
			},
			BackoffLimit: &backOffLimit,
		},
	}

	_, err := jobs.Create(context.TODO(), jobSpec, metav1.CreateOptions{})
	if err != nil {
		fmt.Printf("Error - %#v", err)
		log.Fatalln("Failed to create K8s job.")
	}

	//print job details
	log.Println("Created K8s [" + jobName + "] job successfully")
}

// func deleteJob(ctx context.Context, clientset *kubernetes.Clientset, jobName string) {
// 	err := clientset.BatchV1().Jobs("default").Delete(ctx, jobName, metav1.DeleteOptions{})
// 	if err != nil {
// 		fmt.Printf("Error - %#v", err)
// 		log.Fatalln("Failed to create K8s job.")
// 	}
// 	log.Println("Deleted K8s [" + jobName + "] job successfully")
// }

func main() {
	//ctx := context.Background()
	for {
		jobName := "demo-job-with-envs-"
		containerImage := "ubuntu:latest"
		entryCommand := "ls"
		fmt.Printf("--> jobName:%s\n ", jobName)
		fmt.Printf("--> containerImage:%s \n", containerImage)
		fmt.Printf("--> entryCommand:%s \n", entryCommand)
		flag.Parse()
		clientset := connectToK8s()

		launchK8sJob(clientset, jobName, containerImage, entryCommand)
		time.Sleep(60 * time.Second)
		//deleteJob(ctx, clientset, jobName)
	}
}
