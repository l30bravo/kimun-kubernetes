#App Example:
* [Deployment in Kubernetes](https://youtu.be/q-ZicDSb3Cc)
```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello
  labels:
    role: hello
spec:
  replicas: 3
  selector:
    matchLabels:
      role: hello
      tier: web
  template:
    metadata:
      labels:
        role: hello
        tier: web
    spec:
      containers:
      - name: hello-app
        image: gcr.io/google-samples/hello-app:1.0
        ports:
        - containerPort: 8080
```
# Things to have present
* Into spec->replicas , this means that kubernetes is going to create tree pods with this app.
* Into spec->template->spec->container->images , you can't put your personal image, for example with `docker build` you can create a new image and then you can declare that image in this field

## Deployment

```bash
kubectl apply -f 01-hello-app-deployment.yaml
kubectl get all
kubectl get pods
```

If you need get into one of that pods you can look the configuration `02-hello-app-service-node-port.yaml`
To see the app in the browser, for example, a `nodePort` service must be created, this creates a port on the node that points to the pods

```bash
apiVersion: v1
kind: Service
metadata:
  name: hello
spec:
  type: NodePort
  ports:
  - port: 8080
    targetPort: 8080
    nodePort: 30000
  selector:
    role: hello
```

* The `spect->selector->role`, you can choose any name to represent this nodePort, but you must use the same label with you deploy the app in the script `01-hello-app-deployment` in `spec->template->labels->role`

If you want to see the leabel name relation you cat run this commands
```bash
kubectl get pods
kubectl describe pod {pod-name}
```
In the `Leabels` field you can see `role=hello`, so with this role name the service can find the pod and redirect the traffic network

* applying NodePort
```bash
kubectl apply -f 02-hello-app-service-node-port.yaml
```

* get information about the app
```bash
kubectl describe svc hello
```

* test app
if you are using minikube, you can obtain the ip login into de VM (`minikube ssh`) and running `ifconfig`
```bash
curl http://192.168.99.100:30000
```

* leave pods in quarentine, you need to change the leabel
```bash
kubectl label pod {name-pod} role=quarentine --overwrite
```
